/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rodrigo.calculadoradesaldomedio;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author Diamond
 */
public class CalculadoraDeSaldoMedioTest {
 @Test
    public void deveNaoGerarCredito() {
          double salario = 0 ; 
        CalculadoraDeSaldoMedio calculadora = new CalculadoraDeSaldoMedio();
        double credito = calculadora.calcularSaldoMedido(salario);
        assertEquals(salario * 0, credito, 0.001);

    }

    @Test
    public void deveGerarCreditoDe30PorCento() {
          double salario = 510 ; 
        CalculadoraDeSaldoMedio calculadora = new CalculadoraDeSaldoMedio();
        double credito = calculadora.calcularSaldoMedido(salario);
        assertEquals(salario * 0.3, credito, 0.001);

    }

    @Test
    public void deveGerarCredito40PorCento() {
        CalculadoraDeSaldoMedio calculadora = new CalculadoraDeSaldoMedio();
        double salario = 1100 ; 
        double credito = calculadora.calcularSaldoMedido(salario);
        assertEquals(salario * 0.4, credito, 0.001);

    }
    
     @Test
    public void deveGerarCredito50PorCento() {
        CalculadoraDeSaldoMedio calculadora = new CalculadoraDeSaldoMedio();
        double salario = 3100 ; 
        double credito = calculadora.calcularSaldoMedido(salario);
        assertEquals(salario * 0.5, credito, 0.001);

    }    
}
