/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rodrigo.calculadoradesaldomedio;

/**
 *
 * @author Diamond
 */
public class CalculadoraDeSaldoMedio {
    public double calcularSaldoMedido(double salario) {

        if (salario >= 0 && salario <= 500) {
            return 0;
        } else if (salario < 1001) {
            return salario * 0.3;
        } else if (salario < 3001) {
            return salario * 0.4;
        }

        return salario * 0.5;
    }
    
}
